import pandas as pd
import psycopg2
import os
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)

host = "mazda-datamart.cfisikaor4fw.us-west-2.redshift.amazonaws.com"
port = 5439
dbname = "datamart"
user = "hliu1"
password = "Hl@Mazda2018"

ds012_03_query = """SELECT
	CALENDAR_DATE,
	DEALER_ID,
	DMA_CODE,
	MODEL,
	SUB_MODEL,
	VDP_VIEWS
FROM
	MNAOPROD.DS012_03_SHIFTDIGITAL_DEALERVDPVIEWS_PROD
WHERE
	DMA_NAME <> 'Unknown'
	AND STATUS = 'New'"""

with psycopg2.connect(
    f"host='{host}' port={port} dbname='{dbname}' user={user} password={password}"
) as conn:
    ds012_03_df = pd.read_sql_query(ds012_03_query, conn)
    ds012_03_df.columns = ds012_03_df.columns.str.upper()
    dtypes = {}
    for i in ds012_03_df.columns:
        if i == "CALENDAR_DATE":
            dtypes[i] = DateTime()
        elif i in ["DEALER_ID", "DMA_CODE", "MODEL", "SUB_MODEL"]:
            dtypes[i] = String(200)
        else:
            dtypes[i] = Float()

    ds012_03_df.to_sql(
        "wp_ds012_03_dealervdpviews",
        engine,
        if_exists="replace",
        index=False,
        dtype=dtypes,
        chunksize=20000
    )

import pandas as pd
import psycopg2
import os
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)

host = "mazda-datamart.cfisikaor4fw.us-west-2.redshift.amazonaws.com"
port = 5439
dbname = "datamart"
user = "hliu1"
password = "Hl@Mazda2018"

ds005_01_query = "SELECT * FROM mnaoprod.ds005_01_leads_elms_prod"
ds005_02_query = "SELECT * FROM mnaoprod.ds005_02_leads_matchback_elms_prod"

with psycopg2.connect(
    f"host='{host}' port={port} dbname='{dbname}' user={user} password={password}"
) as conn:
    ds005_01_df = pd.read_sql_query(ds005_01_query, conn)
    ds005_01_df.columns = ds005_01_df.columns.str.upper()
    dtypes = {}
    for i in ds005_01_df.columns:
        if i == "CALENDAR_DATE":
            dtypes[i] = DateTime()
        else:
            dtypes[i] = String(200)

    ds005_01_df.to_sql(
        "wp_ds005_01_leads_elms_prod",
        engine,
        if_exists="replace",
        index=False,
        dtype=dtypes,
        chunksize=20000
    )

    ds005_02_df = pd.read_sql_query(ds005_02_query, conn)
    ds005_02_df.columns = ds005_02_df.columns.str.upper()
    dtypes = {}
    for i in ds005_02_df.columns:
        if i == "CALENDAR_DATE":
            dtypes[i] = DateTime()
        else:
            dtypes[i] = String(200)
    
    ds005_02_df.to_sql(
        "wp_ds005_02_leads_matchback",
        engine,
        if_exists="replace",
        index=False,
        dtype=dtypes,
        chunksize=20000
    )
import pandas as pd
from ftplib import FTP
from io import StringIO
import os
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)

max_date_df = pd.read_sql(
    "SELECT TO_CHAR(MAX(\"Date\") + 2, 'yyyy-mm-dd') AS MAX_DATE FROM wp_shift_vdp_daily",
    engine,
)
max_date = list(max_date_df["max_date"])[0]
max_file = f"MazdaDealerWebsiteVDP_{max_date}.txt"
print(max_file)

with FTP("shiftutils.cloudapp.net") as ftp:
    ftp.login("mazda-webmetrics", "qs79Gh&#")
    try:
        content = []
        ftp.retrlines(f"RETR {max_file}", content.append)
        result = StringIO("\n".join(content))
        vdp_df = pd.read_csv(result, sep="\t", engine="python", error_bad_lines=False)
        vdp_df["SourceFileName"] = max_file
        vdp_df["Date"] = pd.to_datetime(vdp_df["Date"])
        dtypes = {
            "Date": DateTime(),
            "ProviderId": String(200),
            "RetailerId": String(200),
            "Make": String(200),
            "Model": String(200),
            "Trim": String(200),
            "Status": String(200),
            "VdpViews": Float(),
            "SourceFileName": String(200),
        }
        vdp_df.to_sql(
            "wp_shift_vdp_daily", engine, if_exists="append", index=False, dtype=dtypes,
        )
        print(f"Success writing file: {max_file}")
    except Exception as e:
        print(e)


# with FTP("shiftutils.cloudapp.net") as ftp:
#     ftp.login("mazda-webmetrics", "qs79Gh&#")
#     ## get directory listing
#     for i in ftp.nlst():
#         if i not in list(source_existing_files["SourceFileName"]):
#             if "VDP" in i and i.endswith(".txt"):
#                 content = []
#                 ftp.retrlines(f"RETR {i}", content.append)
#                 result = StringIO("\n".join(content))
#                 print(i)
#                 # vdp_df = pd.read_csv(result, sep="\t", engine="python", error_bad_lines=False)
#                 # vdp_df["SourceFileName"] = i
#                 # vdp_df["Date"] = pd.to_datetime(vdp_df["Date"])
#                 # dtypes = {
#                 #     "Date": DateTime(),
#                 #     "ProviderId": String(200),
#                 #     "RetailerId": String(200),
#                 #     "Make": String(200),
#                 #     "Model": String(200),
#                 #     "Trim": String(200),
#                 #     "Status": String(200),
#                 #     "VdpViews": Float(),
#                 #     "SourceFileName": String(200),
#                 # }
#                 # vdp_df.to_sql(
#                 #     "wp_shift_vdp_daily", engine, if_exists="append", index=False, dtype=dtypes,
#                 # )
#         ## call pwd just to make sure the connection isn't closed
#         ftp.pwd()
